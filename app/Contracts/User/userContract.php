<?php

namespace App\Contracts\User;

interface userContract
{
    /**
     * @return mixed
     */
    public function findUser(int $id);

    /**
     * @return mixed
     */
    public function createUser(array $arr);

    /**
     * @return mixed
     */
    public function updateUser(array $arr, int $id);

    /**
     * @return mixed
     */
    public function listUsers(array $fields = [], $columns = array('*'), string $orderBy = 'id', string $sortBy = 'desc');
}
