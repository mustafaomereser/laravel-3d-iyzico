<?php

namespace App\Repositories\User;

use App\Contracts\User\userContract;
use App\Models\User;
use App\Repositories\BaseRepository;


/**
 * Class UserRepository.
 * @package \App\Repositories\User
 */

class UserRepository extends BaseRepository implements userContract
{

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->model = $user;
    }

    /**
     * @return mixed
     */
    public function findUser(int $id)
    {
        return $this->find($id);
    }

    /**
     * @return mixed
     */
    public function createUser(array $arr)
    {
        return $this->create($arr);
    }

    /**
     * @return mixed
     */
    public function updateUser(array $arr, int $id)
    {
        return $this->update($arr, $id);
    }

    /**
     * @return mixed
     */
    public function listUsers(array $fields = [], $columns = array('*'), string $orderBy = 'id', string $sortBy = 'desc')
    {
        return $this->all($fields, $columns, $orderBy, $sortBy);
    }
}
